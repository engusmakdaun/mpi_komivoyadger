import logging
import time

import numpy as np
import komivoyadger as kom
from mpi4py import MPI

logger = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)s - %(levelname)s - %(message)s',
    datefmt='%m-%d-%Y %I:%M:%S'
)


def main():

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    if rank == 0:
        # Корневой процесс генерирует массив с номерами городов и разбивает его на равные части
        town_array = np.arange(len(kom.X))
        main_list = (list([list(ar) for ar in np.array_split(town_array, size)]))
    else:
        main_list = None

    # Распределение частей main_list по процессам
    ways_range = comm.scatter(main_list, root=0)

    # поиск кратчайшего пути на заданной последовательности
    min_way = kom.get_way(ways_range)

    # отправка данных в корневой процесс
    min_way_list = comm.gather(min_way, root=0)

    logger.info(min_way_list)

    if rank == 0:

        # Корневой процесс получив данные собирает их в 2 массива и находит никратчайший путь из тех,
        # что были найдены другими процессами

        RS = []
        RIB = []

        for i in range(0, size):

            RS.append(min_way_list[i][0])
            RIB.append(min_way_list[i][1])

        S, ib = min(RS), RIB[RS.index(min(RS))]

        logger.info(f"The shortest route - {S}, at the beginning of the movement from the point - {ib}")


if __name__ == '__main__':
    start = time.time()

    main()

    end = time.time()
    # подсчет затраченного времени на выполнение программы
    logger.info(f"The time of execution of above is {(end - start) * 10**3} ms")