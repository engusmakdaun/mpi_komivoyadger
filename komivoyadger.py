import numpy as np
from numpy import sqrt

np.random.seed(1)

n = 250
m = 150
a = 0

X = list(np.round(np.random.uniform(a, m, n), 0))
Y = list(np.round(np.random.uniform(a, m, n), 0))


def get_way(range_start_town):
    RS, RW, RIB, way = [], [], [], []

    for start_town in range_start_town:

        M = np.zeros([n, n])

        for i in np.arange(0, n):
            for j in np.arange(0, n):
                if i != j:
                    M[i, j] = sqrt((X[i] - X[j]) ** 2 + (Y[i] - Y[j]) ** 2)
                else:
                    M[i, j] = float('inf')

        way = [start_town]

        for i in np.arange(1, n):

            s = []

            for j in np.arange(0, n):
                s.append(M[way[i - 1], j])

            way.append(s.index(min(s)))

            for j in np.arange(0, i):
                M[way[i], way[j]] = float('inf')
                M[way[i], way[j]] = float('inf')

        S = sum([sqrt((X[way[i]] - X[way[i + 1]]) ** 2 + (Y[way[i]] - Y[way[i + 1]]) ** 2) for i in
                 np.arange(0, n - 1, 1)]) + sqrt((X[way[n - 1]] - X[way[0]]) ** 2 + (Y[way[n - 1]] - Y[way[0]]) ** 2)

        RS.append(S)
        RIB.append(start_town)

    return min(RS), RIB[RS.index(min(RS))]